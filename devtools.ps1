$ErrorActionPreference = "Continue"
if ( -Not (get-command scoop)) 
 {
    irm get.scoop.sh  | iex 
}

[int]$isInstalled = -1
Function testPackage ($packageName){
[string]$test = ""
   try {
    write-host "check $packageName"
    $test = winget list --id $packageName   --source winget   
 
} catch {
    $test = ""
}
if ($test = "") {
             $isInstalled = 1
         }
         else {
             $isInstalled = 0
         }
         return $isInstalled
}


Function createShortcut  {
	param (
	[Parameter(Mandatory=$true, Position=0)]
		[string]$name,
	 [Parameter(Mandatory=$true, Position=1)]
		[string]$target,
	 [Parameter(Mandatory=$true, Position=2)]
		[string]$param
	)
 
$destination = [Environment]::GetFolderPath("Desktop")
$shell = New-Object -COM WScript.Shell
$shortcut = $shell.CreateShortcut(( $destination + "\$name.lnk")) 
$shortcut.TargetPath = $target
$shortcut.Arguments= $param
     
$shortcut.WorkingDirectory = split-path ($target) 
$shortcut.Description = $name
$shortcut.Save()  
write-host ("create shortcut " + $name + ":" + $target + ","+ ($destination + "$name.lnk"))
}

Function Test-CommandExists
{
 Param ($command)
 $oldPreference = $ErrorActionPreference
 $ErrorActionPreference = "stop"
 try {if(Get-Command $command){"$command exists"}}
 Catch {"$command does not exist"}
 Finally {$ErrorActionPreference=$oldPreference}
} 

$toInstall = @(
					"Git.Git",
					"Microsoft.OpenJDK.11",
					"OpenJS.NodeJS.LTS",
 
					"TortoiseGit.TortoiseGit",
					"Microsoft.VisualStudioCode",
 
					"HeidiSQL.HeidiSQL",
					"WinMerge.WinMerge")
     foreach ($utility in $toInstall) {          #$toInstall is a string array with all the id of the packages that i need
         if (testPackage($utility) -eq 1) {
             write-host "installing $utility..."
             winget install -e --id $utility  --source winget      
         } 
         else {
             Write-Host "$utility already installed";
			 winget upgrade   $utility  --source winget 
         }
     }
	 

$java_home=split-path (split-path ((get-command java.exe).source))
[System.Environment]::SetEnvironmentVariable('JAVA_HOME',$java_home,[System.EnvironmentVariableTarget]::User)


Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
 
 
    scoop update *

 
scoop bucket add extras
scoop bucket add java
scoop install  netbeans 

scoop install maven 
scoop install Yarn
scoop install MariaDB
npm install @angular/cli@13 --global

createShortcut -name "Apache Netbeans" -target  "$env:USERPROFILE\scoop\apps\netbeans\current\bin\netbeans64.exe" -param '--jdkhome "%JAVA_HOME%"'  
createShortcut -name "Visual Studio Code" -target  ((split-path(split-path ((get-command code).source)))+"\Code.exe") -param ' '  

